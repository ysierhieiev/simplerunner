// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TestRunnerGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class TESTRUNNER_API ATestRunnerGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
