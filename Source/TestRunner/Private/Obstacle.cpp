#include "Obstacle.h"
#include "RunnerCharacter.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"

AObstacle::AObstacle()
{
 	PrimaryActorTick.bCanEverTick = false;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(RootComponent);

	DefeatTrigger = CreateDefaultSubobject<UBoxComponent>(TEXT("DefeatTrigger"));
	DefeatTrigger->SetupAttachment(Mesh);
}

void AObstacle::BeginPlay()
{
	Super::BeginPlay();

	Mesh->OnComponentHit.AddDynamic(this, &AObstacle::MeshOnHit);
	DefeatTrigger->OnComponentBeginOverlap.AddDynamic(this, &AObstacle::DefeatTriggerOnPlayerBeginOverlap);
}

void AObstacle::MeshOnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor->IsA(ARunnerCharacter::StaticClass()))
	{
		auto Player = Cast<ARunnerCharacter>(OtherActor);
		Player->CancelStrafe();
	}
}

void AObstacle::DefeatTriggerOnPlayerBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor->IsA(ARunnerCharacter::StaticClass()))
	{
		auto Player = Cast<ARunnerCharacter>(OtherActor); 
		Player->DefeatPlayer();
	}
}


