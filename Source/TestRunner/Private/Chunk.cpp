#include "Chunk.h"
#include "RunnerGameMode.h"
#include "RunnerCharacter.h"
#include "Obstacle.h"
#include "Engine/World.h"
#include "TimerManager.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"

AChunk::AChunk()
{
 	PrimaryActorTick.bCanEverTick = false;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));

	Floor = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Floor"));
	Floor->SetupAttachment(RootComponent);
	Floor->SetRelativeLocation(FVector(500.f, 0.f, 0.f));
	Floor->RelativeScale3D = FVector(10.f, 10.f, 1.f);

	SpawnPoint = CreateDefaultSubobject<UArrowComponent>(TEXT("SpawnPoint"));
	SpawnPoint->SetupAttachment(RootComponent);
	SpawnPoint->SetRelativeLocation(FVector(1000.f, 0.f, 0.f));

	SpawnObstaclePointLeft = CreateDefaultSubobject<UArrowComponent>(TEXT("SpawnObstaclePointLeft"));
	SpawnObstaclePointLeft->SetupAttachment(RootComponent);

	SpawnObstaclePointMiddle = CreateDefaultSubobject<UArrowComponent>(TEXT("SpawnObstaclePointMiddle"));
	SpawnObstaclePointMiddle->SetupAttachment(RootComponent);

	SpawnObstaclePointRight = CreateDefaultSubobject<UArrowComponent>(TEXT("SpawnObstaclePointRight"));
	SpawnObstaclePointRight->SetupAttachment(RootComponent);

	DestroyTrigger = CreateDefaultSubobject<UBoxComponent>(TEXT("DestroyTrigger"));
	DestroyTrigger->SetupAttachment(RootComponent);
	DestroyTrigger->SetRelativeLocation(FVector(970.f, 0.f, 70.f));
	DestroyTrigger->RelativeScale3D = FVector(1.f, 15.5f, 2.f);

}

void AChunk::BeginPlay()
{
	Super::BeginPlay();
	
	DestroyTrigger->OnComponentBeginOverlap.AddDynamic(this, &AChunk::DestroyTriggerOnPlayerBeginOverlap);

	if (ObstacleClass != nullptr)
	{
		switch (FMath::RandRange((int16)0, (int16)3))
		{
		case 0: break;
		case 1:
			CurrentObstacle = GetWorld()->SpawnActor<AObstacle>(ObstacleClass, SpawnObstaclePointLeft->GetComponentTransform());
			break;
		case 2:
			CurrentObstacle = GetWorld()->SpawnActor<AObstacle>(ObstacleClass, SpawnObstaclePointMiddle->GetComponentTransform());
			break;
		case 3:
			CurrentObstacle = GetWorld()->SpawnActor<AObstacle>(ObstacleClass, SpawnObstaclePointRight->GetComponentTransform());
			break;
		}
	}	
}


void AChunk::DestroyTriggerOnPlayerBeginOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor->IsA(ARunnerCharacter::StaticClass()))
	{
		ARunnerGameMode* CurrentGameMode = Cast<ARunnerGameMode>(GetWorld()->GetAuthGameMode());
		if (CurrentGameMode)
		{
			CurrentGameMode->CreateChunk();
			CurrentGameMode->IncreaseScore();
			GetWorldTimerManager().SetTimer(TimerHandle_TimeToSelfDestroy, this, &AChunk::DestroyChunk, 2.f);
		}
	}
}

void AChunk::DestroyObstacle()
{
	if (CurrentObstacle)
	{
		CurrentObstacle->Destroy();
	}
}

void AChunk::DestroyChunk()
{
	if (this->IsValidLowLevel())
	{
		DestroyObstacle();
		this->Destroy();
	}
}

