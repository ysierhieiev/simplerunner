// Fill out your copyright notice in the Description page of Project Settings.


#include "TurnChunk.h"
#include "RunnerGameMode.h"
#include "RunnerCharacter.h"
#include "Obstacle.h"
#include "Engine/World.h"
#include "TimerManager.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"

ATurnChunk::ATurnChunk()
{
 	PrimaryActorTick.bCanEverTick = false;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));

	Floor = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Floor"));
	Floor->SetupAttachment(RootComponent);
	Floor->SetRelativeLocation(FVector(500.f, 0.f, 0.f));
	Floor->RelativeScale3D = FVector(10.f, 10.f, 1.f);

	Wall = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Wall"));
	Wall->SetupAttachment(RootComponent);
	auto WallTransform = FTransform(FRotator(0.f, 90.f, 0.f), FVector(1000.f, -500.f, 0.f),  FVector(2.5f, 1.f, 0.75f));
	Wall->SetRelativeTransform(WallTransform);

	SideWall = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SideWall"));
	SideWall->SetupAttachment(RootComponent);
	SideWall->RelativeScale3D = FVector(2.5f, 1.f, 0.75f);

	SpawnPoint = CreateDefaultSubobject<UArrowComponent>(TEXT("SpawnPoint"));
	SpawnPoint->SetupAttachment(RootComponent);
	
	DestroyTrigger = CreateDefaultSubobject<UBoxComponent>(TEXT("DestroyTrigger"));
	DestroyTrigger->SetupAttachment(RootComponent);
	auto TriggerTransform = FTransform(FRotator(0.f, 90.f, 0.f), FVector(500, 0.f, 70.f), FVector(1.f, 15.5f, 2.f));
	DestroyTrigger->SetRelativeTransform(TriggerTransform);

	TurnTrigger = CreateDefaultSubobject<UBoxComponent>(TEXT("TurnTrigger"));
	TurnTrigger->SetupAttachment(RootComponent);
	TurnTrigger->SetRelativeLocation(FVector(480.f, 0.f, 10.f));
	TurnTrigger->RelativeScale3D = FVector(14.f, 14.f, 0.25f);
}

void ATurnChunk::BeginPlay()
{
	Super::BeginPlay();
	
	Wall->OnComponentHit.AddDynamic(this, &ATurnChunk::OnHitWall);
	SideWall->OnComponentHit.AddDynamic(this, &ATurnChunk::OnHitWall);

	DestroyTrigger->OnComponentBeginOverlap.AddDynamic(this, &ATurnChunk::DestroyTriggerOnPlayerBeginOverlap);
	TurnTrigger->OnComponentBeginOverlap.AddDynamic(this, &ATurnChunk::TurnTriggerOnPlayerBeginOverlap);

	if (FMath::RandBool())
	{
		SpawnPoint->SetRelativeLocation(FVector(500.f, 500.f, 0.f));
		SpawnPoint->SetRelativeRotation(FRotator(0.f, 90.f, 0.f));

		SideWall->SetRelativeLocation(FVector(0.f, -510.f, 0.f));

		DestroyTrigger->SetRelativeLocation(FVector(500.f, 480.f, 70.f));
	}
	else
	{
		SpawnPoint->SetRelativeLocation(FVector(500.f, -500.f, 0.f));
		SpawnPoint->SetRelativeRotation(FRotator(0.f, -90.f, 0.f));

		SideWall->SetRelativeLocation(FVector(0.f, 510.f, 0.f));

		DestroyTrigger->SetRelativeLocation(FVector(500.f, -480.f, 70.f));
	}
}


void ATurnChunk::OnHitWall(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor->IsA(ARunnerCharacter::StaticClass()))
	{
		auto Player = Cast<ARunnerCharacter>(OtherActor);
		Player->DefeatPlayer();
	}
}

void ATurnChunk::DestroyTriggerOnPlayerBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor->IsA(ARunnerCharacter::StaticClass()))
	{
		ARunnerGameMode* CurrentGameMode = Cast<ARunnerGameMode>(GetWorld()->GetAuthGameMode());
		if (CurrentGameMode)
		{
			CurrentGameMode->CreateChunk();
			CurrentGameMode->IncreaseScore();
			GetWorldTimerManager().SetTimer(TimerHandle_TimeToSelfDestroy, this, &ATurnChunk::DestroyTurnChunk, 2.f);
		}
	}
}

void ATurnChunk::TurnTriggerOnPlayerBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor->IsA(ARunnerCharacter::StaticClass()))
	{
		auto Player = Cast<ARunnerCharacter>(OtherActor);
		Player->ActiveTurn(SpawnPoint->GetComponentLocation());
	}
}

void ATurnChunk::DestroyTurnChunk()
{
	if (this->IsValidLowLevel())
	{
		this->Destroy();
	}
}
