// Fill out your copyright notice in the Description page of Project Settings.

#include "RunnerCharacter.h"
#include "Components/InputComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "TimerManager.h"

ARunnerCharacter::ARunnerCharacter()
{
 	PrimaryActorTick.bCanEverTick = true;
	
	SpringArmComp = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArmComp"));
	SpringArmComp->bUsePawnControlRotation = true;
	SpringArmComp->SetupAttachment(RootComponent);

	CameraComp = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComp"));
	CameraComp->SetupAttachment(SpringArmComp);

	LaneCoords.SetNum(3);
}

void ARunnerCharacter::BeginPlay()
{
	Super::BeginPlay();

	GetWorldTimerManager().SetTimer(TimerHandle_TimeToStart, this, &ARunnerCharacter::UpdateTimerToStart, 1.f, true);

	ZeroCoord = GetActorLocation();

	CalculateRunLines(GetActorLocation());	
}

void ARunnerCharacter::LeftStrafe()
{
	if (isCanTurn)
	{		
		DesiredRotation += FRotator(0.f, -90.f, 0.f);
		isCanTurn = false;
		isXCoord == true ? isXCoord = false : isXCoord = true;
		RotateCompasDirection(false);
		CalculateRunLines(ZeroCoord);
	}
	else
	{
		if (CurrentLaneIndex == 0) return;
		LastLaneIndex = CurrentLaneIndex;
		--CurrentLaneIndex;
	}
}

void ARunnerCharacter::RightStrafe()
{
	if (isCanTurn)
	{
		DesiredRotation += FRotator(0.f, +90.f, 0.f);
		isCanTurn = false;
		isXCoord == true ? isXCoord = false : isXCoord = true;
		RotateCompasDirection(true);
		CalculateRunLines(ZeroCoord);
	}
	else
	{
		if (CurrentLaneIndex == 2) return;
		LastLaneIndex = CurrentLaneIndex;
		++CurrentLaneIndex;
	}
}

void ARunnerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (isCanRun && !isDead)
	{

		if (GetControlRotation() != DesiredRotation)
		{
			GetController()->SetControlRotation(FMath::RInterpTo(GetControlRotation(), DesiredRotation, DeltaTime, 10.f));
		}


		AlignmentCharacter(DeltaTime);

		AddMovementInput(GetActorForwardVector());
	}
}

void ARunnerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAction("LeftStrafe", IE_Pressed, this, &ARunnerCharacter::LeftStrafe);
	PlayerInputComponent->BindAction("RightStrafe", IE_Pressed, this, &ARunnerCharacter::RightStrafe);
}

void ARunnerCharacter::CancelStrafe()
{
	CurrentLaneIndex = LastLaneIndex;
}

void ARunnerCharacter::DefeatPlayer_Implementation()
{
	isDead = true;
}

void ARunnerCharacter::ActiveTurn(FVector MiddleLocation)
{
	isCanTurn = true;	
	ZeroCoord = MiddleLocation;
}

void ARunnerCharacter::UpdateTimerToStart()
{
	--TimeToStart;

	if (TimeToStart == 0)
	{
		isCanRun = true;
	}
	if (TimeToStart < 0)
	{
		GetWorldTimerManager().ClearTimer(TimerHandle_TimeToStart);
	}
}

void ARunnerCharacter::RotateCompasDirection(bool isRightRotate)
{
	if (isRightRotate)
	{
		switch (RunnerDirection)
		{
		case Direction::D_North: 
			RunnerDirection = Direction::D_East;
			break;
		case Direction::D_East: 
			RunnerDirection = Direction::D_South;
			break;
		case Direction::D_South:
			RunnerDirection = Direction::D_West;
			break;
		case Direction::D_West:
			RunnerDirection = Direction::D_North;
			break;
		}
	}
	else
	{
		switch (RunnerDirection)
		{
		case Direction::D_North:
			RunnerDirection = Direction::D_West;
			break;
		case Direction::D_East:
			RunnerDirection = Direction::D_North;
			break;
		case Direction::D_South:
			RunnerDirection = Direction::D_East;
			break;
		case Direction::D_West:
			RunnerDirection = Direction::D_South;
			break;
		}
	}
}

void ARunnerCharacter::AlignmentCharacter(float DeltaTime)
{
	auto PlayerLocation = GetActorLocation();
	if(isXCoord)
	{
		if(PlayerLocation.Y != LaneCoords[CurrentLaneIndex])
		{
			float DeltaCoord = FMath::FInterpTo(PlayerLocation.Y, LaneCoords[CurrentLaneIndex], DeltaTime, 10.f);
			PlayerLocation.Y = DeltaCoord;
		}
	}
	else
	{
		if (PlayerLocation.X != LaneCoords[CurrentLaneIndex])
		{
			float DeltaCoord = FMath::FInterpTo(PlayerLocation.X, LaneCoords[CurrentLaneIndex], DeltaTime, 10.f);
			PlayerLocation.X = DeltaCoord;
		}
	}
	SetActorLocation(PlayerLocation);
}

void ARunnerCharacter::CalculateRunLines(FVector ZeroCoord)
{
	float RightButtonSide = 1.f;
	if (RunnerDirection == Direction::D_East || RunnerDirection == Direction::D_South)
	{
		RightButtonSide *= -1.f;
	}
	if (isXCoord)
	{
		LaneCoords[1] = ZeroCoord.Y;
		LaneCoords[0] = ZeroCoord.Y - (RightButtonSide * DistanceBetweenLine);
		LaneCoords[2] = ZeroCoord.Y + (RightButtonSide * DistanceBetweenLine);
	}								  
	else							  
	{								  
		LaneCoords[1] = ZeroCoord.X; 
		LaneCoords[0] = ZeroCoord.X - (RightButtonSide * DistanceBetweenLine);
		LaneCoords[2] = ZeroCoord.X + (RightButtonSide * DistanceBetweenLine);
	}
}

