// Fill out your copyright notice in the Description page of Project Settings.


#include "RunnerGameMode.h"
#include "Engine/World.h"
#include "Chunk.h"
#include "TurnChunk.h"
#include "UObject/ConstructorHelpers.h"
#include "TimerManager.h"

ARunnerGameMode::ARunnerGameMode()
{
	ConstructorHelpers::FClassFinder<AChunk> ChunkClass(TEXT("/Game/Blueprints/Chunks/BP_Chunk"));
	if (!ensure(ChunkClass.Class != nullptr)) return;

	BPChunkClass = ChunkClass.Class;
	
	ConstructorHelpers::FClassFinder<ATurnChunk> TurnChunkClass(TEXT("/Game/Blueprints/Chunks/BP_TurnChunk"));
	if (!ensure(TurnChunkClass.Class != nullptr)) return;

	BPTurnChunkClass = TurnChunkClass.Class;
}

void ARunnerGameMode::BeginPlay()
{
	Super::BeginPlay();

	NextSpawnLocation = FTransform(FVector::ZeroVector);

	NeedChunksForTurn = FMath::RandRange(8, 15);

	for (auto i = 0u; i < 15; ++i)
	{
		auto CurrentChunk = GetWorld()->SpawnActor<AChunk>(BPChunkClass, NextSpawnLocation);
		NextSpawnLocation = CurrentChunk->GetSpawnPointLocation();
		if (i < 3u)
		{
			CurrentChunk->DestroyObstacle();
		}
		++CountOfChunk;
	}

}

void ARunnerGameMode::CreateChunk()
{
	if (CountOfChunk < NeedChunksForTurn)
	{
		auto CurrentChunk = GetWorld()->SpawnActor<AChunk>(BPChunkClass, NextSpawnLocation);
		NextSpawnLocation = CurrentChunk->GetSpawnPointLocation();
		++CountOfChunk;
	}
	else
	{
		auto CurrentChunk = GetWorld()->SpawnActor<ATurnChunk>(BPTurnChunkClass, NextSpawnLocation);
		NextSpawnLocation = CurrentChunk->GetSpawnPointLocation();
		CountOfChunk = 0;
		NeedChunksForTurn = FMath::RandRange(8, 15);
	}
}

void ARunnerGameMode::IncreaseScore()
{
	PlayerScore += PointMultiplier;
}
