#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RunnerGameMode.generated.h"
 
UCLASS()
class TESTRUNNER_API ARunnerGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	
	ARunnerGameMode();

	virtual void BeginPlay() override;

	void CreateChunk();

	void IncreaseScore();

protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Variables")
	TSubclassOf<class AChunk> BPChunkClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Variables")
	TSubclassOf<class ATurnChunk> BPTurnChunkClass;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Variables")
	FTransform NextSpawnLocation;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Variables")
	int32 CountOfChunk = 0;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Variables")
	int32 NeedChunksForTurn;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Variables")
	int32 PlayerScore = 0;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Variables")
	int32 PointMultiplier = 1;	   
};
