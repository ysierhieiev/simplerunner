#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "RunnerCharacter.generated.h"

UENUM()
enum Direction
{
	D_North UMETA(DisplayName = "North"),
	D_East  UMETA(DisplayName = "East"),
	D_West  UMETA(DisplayName = "West"),
	D_South UMETA(DisplayName = "South")
};

UCLASS()
class TESTRUNNER_API ARunnerCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ARunnerCharacter();

	virtual void Tick(float DeltaTime) override;

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void CancelStrafe();

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Character")
	void DefeatPlayer();

	void ActiveTurn(FVector MiddleLocation);

protected:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	class USpringArmComponent* SpringArmComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	class UCameraComponent* CameraComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Variables")
	TEnumAsByte<Direction> RunnerDirection = Direction::D_North;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Variables")
	FVector ZeroCoord;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Variables")
	TArray<float> LaneCoords;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Variables")
	float DistanceBetweenLine = 300.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Variables")
	int32 TimeToStart = 3;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Variables")
	int32 CurrentLaneIndex = 1;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Variables")
	int32 LastLaneIndex = 1;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	bool isXCoord = true;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	bool isCanRun = false;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	bool isCanTurn = false;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
	bool isDead = false;

	virtual void BeginPlay() override;

	void LeftStrafe();

	void RightStrafe();

private:

	FTimerHandle TimerHandle_TimeToStart;

	FRotator DesiredRotation;

	void UpdateTimerToStart();

	void RotateCompasDirection(bool isRightRotate);

	void AlignmentCharacter(float DeltaTime);
	
	void CalculateRunLines(FVector ZeroCoord);
};
