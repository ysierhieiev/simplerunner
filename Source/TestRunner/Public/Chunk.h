#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"
#include "Chunk.generated.h"

UCLASS()
class TESTRUNNER_API AChunk : public AActor
{
	GENERATED_BODY()
	
public:	

	AChunk();
	   
	UFUNCTION()
	void DestroyTriggerOnPlayerBeginOverlap(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	FORCEINLINE FTransform GetSpawnPointLocation() { return SpawnPoint->GetComponentTransform(); }

	UFUNCTION(BlueprintCallable, Category = "Obstacle")
	void DestroyObstacle();

protected:
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	UStaticMeshComponent* Floor;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	UArrowComponent* SpawnPoint;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	UArrowComponent* SpawnObstaclePointLeft;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	UArrowComponent* SpawnObstaclePointMiddle;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	UArrowComponent* SpawnObstaclePointRight;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	class UBoxComponent* DestroyTrigger;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Variables")
	TSubclassOf<class AObstacle> ObstacleClass;
	
private:

	AObstacle* CurrentObstacle;

	FTimerHandle TimerHandle_TimeToSelfDestroy;

	void DestroyChunk();
};
